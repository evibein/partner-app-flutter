import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:partnerappflutter/app/database_helper.dart';
import 'package:partnerappflutter/app/user.dart';
import 'package:partnerappflutter/app/login_screen_presenter.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new LoginScreenState();
  }
}

class LoginScreenState extends State<LoginScreen> implements LoginScreenContract {
  String _username, _password;

  bool _isLoading = false;

  final scaffoldKey = new GlobalKey<ScaffoldState>();
  final formKey = new GlobalKey<FormState>();

  Widget _buildEmailTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Email',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            onChanged: (val) => _username = val,
            keyboardType: TextInputType.emailAddress,
            style: TextStyle(
              color: Colors.black54,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.email,
                color: Colors.black54,
              ),
              hintText: 'Enter your Email',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildPasswordTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          'Password',
          style: kLabelStyle,
        ),
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          decoration: kBoxDecorationStyle,
          height: 60.0,
          child: TextField(
            onChanged: (val) => _password = val,
            obscureText: true,
            style: TextStyle(
              color: Colors.black54,
              fontFamily: 'OpenSans',
            ),
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(top: 14.0),
              prefixIcon: Icon(
                Icons.lock,
                color: Colors.black54,
              ),
              hintText: 'Enter your Password',
              hintStyle: kHintTextStyle,
            ),
          ),
        ),
      ],
    );
  }

  Widget _buildLoginBtn() {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 25.0),
      width: double.infinity,
      child: RaisedButton(
        elevation: 5.0,
        onPressed: () => _submit(),
        padding: EdgeInsets.all(15.0),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30.0),
        ),
        color: Color(0xFFED3D72),
        child: Text(
          'LOGIN',
          style: TextStyle(
            color: Colors.white,
            letterSpacing: 1.5,
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
            fontFamily: 'OpenSans',
          ),
        ),
      ),
    );
  }

  LoginScreenPresenter _presenter;

  LoginScreenState() {
    _presenter = new LoginScreenPresenter(this);
  }

  void _showSnackBar(String text) {
    scaffoldKey.currentState.showSnackBar(new SnackBar(content: new Text(text)));
  }

  void _submit() {
    if (_username == null) {
      _showSnackBar("Please enter your Email Address");
    } else if (!_username.contains("@")) {
      _showSnackBar("Please enter a valid email address");
    } else if (_password == null) {
      _showSnackBar("Please enter your Password");
    } else if (_password.length < 6) {
      _showSnackBar("Password must contain at least 6 charecters");
    } else {
      setState(() => _isLoading = true);
      _presenter.doLogin(_username, _password);
    }
  }

  void onLoginError(String errorTxt) {
    _showSnackBar(errorTxt.replaceFirst("Exception:", ""));
    setState(() => _isLoading = false);
  }

  final flutterWebViewPlugin = FlutterWebviewPlugin();

  // Rest apis login success handling
  void onLoginSuccess(User user) async {
    var db = new DatabaseHelper();
    await db.saveUser(user);

    flutterWebViewPlugin.launch(
      "https://evibe.in/customer/login/app?uId=" + user.username + "&access-token=" + user.password,
      rect: Rect.fromLTWH(0.0, 1.00, 1.00, 1.00),
    );
  }

  @override
  Widget build(BuildContext context) {
    flutterWebViewPlugin.onStateChanged.listen((viewState) async {
      if (viewState.type == WebViewState.shouldStart) {
        if (viewState.url.startsWith("https://evibe.in/?ref=cusLoginTrue")) {
          flutterWebViewPlugin.close();
          setState(() {
            Navigator.pushNamed(context, '/home');
          });
        }
      }
    });

    return new WillPopScope(
      onWillPop: () async => Future.value(false),
      child: new Scaffold(
        key: scaffoldKey,
        body: AnnotatedRegion<SystemUiOverlayStyle>(
          value: SystemUiOverlayStyle.light,
          child: GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Stack(
              alignment: AlignmentDirectional.center,
              children: <Widget>[
                Container(
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                      begin: Alignment.topCenter,
                      end: Alignment.bottomCenter,
                      colors: [
                        Color(0xFFFFFFFF),
                        Color(0xFFFFFFFF),
                        Color(0xFFFFFFFF),
                        Color(0xFFFFFFFF),
                      ],
                      stops: [0.1, 0.4, 0.7, 0.9],
                    ),
                  ),
                ),
                Container(
                  child: SingleChildScrollView(
                    padding: EdgeInsets.symmetric(
                      horizontal: 40.0,
                      vertical: 120.0,
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          width: MediaQuery.of(context).size.width * 0.3,
                          child: Image.asset('assets/images/logo_company.png'),
                        ),
                        SizedBox(height: 55.0),
                        _buildEmailTF(),
                        SizedBox(height: 20.0),
                        _buildPasswordTF(),
                        SizedBox(height: 25.0),
                        _isLoading ? new CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Color(0xFFed3e72))) : _buildLoginBtn(),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}

final kHintTextStyle = TextStyle(
  color: Colors.black54,
  fontFamily: 'OpenSans',
);

final kLabelStyle = TextStyle(
  color: Colors.black54,
  fontWeight: FontWeight.bold,
  fontFamily: 'OpenSans',
);

final kBoxDecorationStyle = BoxDecoration(
  color: Color(0xFFFFFFFF),
  borderRadius: BorderRadius.circular(10.0),
  boxShadow: [
    BoxShadow(
      color: Colors.black12,
      blurRadius: 6.0,
      offset: Offset(0, 2),
    ),
  ],
);
