import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final flutterWebViewPlugin = FlutterWebviewPlugin();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  var _curIndex = 0;
  var contents = "Dashboard";

  File _imageFile;
  DateTime backButtonPressTime;
  String isLaunch = "1";

  static const EVIBE_PINK = Color(0xFFED3D72);
  static const BLACK_LIGHT = Color(0xFFffffff);
  static const kAndroidUserAgent = 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) wv AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Mobile Safari/537.36';

  void _checkRouteAndOpenWebView(BuildContext context) {
    Navigator.popUntil(context, (route) {
      if (route.settings.name != "/home") {
        String selectedUrl = "https://evibe.in/partner/" + route.settings.name;
        if (isLaunch == "2") {
          print("webview load1");
          flutterWebViewPlugin.reloadUrl(selectedUrl);
        } else {
          print("webview load1");
          flutterWebViewPlugin.launch(selectedUrl,
              rect: Rect.fromLTWH(0.0, MediaQuery.of(context).padding.top, MediaQuery.of(context).size.width + 2.00, MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top + 2.00), userAgent: kAndroidUserAgent, scrollBar: false);
        }
        isLaunch = "2";
      } else {
        print("webview load3");
        flutterWebViewPlugin.launch("https://evibe.in/partner/dashboard",
            rect: Rect.fromLTWH(0.0, MediaQuery.of(context).padding.top + 56, MediaQuery.of(context).size.width, MediaQuery.of(context).size.height - MediaQuery.of(context).padding.top - 116.00), userAgent: kAndroidUserAgent, scrollBar: false);
        isLaunch = "2";
      }
      return true;
    });
  }

  /// Remove image
  void _startUpload() {
    File file = _imageFile;
    if (file == null) return;
    String base64Image = base64Encode(file.readAsBytesSync());
    String fileName = file.path.split("/").last;

    http.post("http://192.168.0.144:8080/partner/v1/deliveries/62903/upload", body: {
      "attachments": base64Image,
      "name": fileName,
    }).then((res) {
      print(res.statusCode);
      print(fileName);
      Navigator.of(context).pop(null);
      flutterWebViewPlugin.show();
    }).catchError((err) {
      print(err);
    });
  }

  @override
  void initState() {
    super.initState();

    flutterWebViewPlugin.close();

    flutterWebViewPlugin.onStateChanged.listen((viewState) async {
      print(viewState.type);
      print(viewState.url);
      if (viewState.type == WebViewState.shouldStart) {
        if (viewState.url.startsWith("https://evibe.in/partner/deliveries/open-camera")) {
          setState(() {
            flutterWebViewPlugin.stopLoading();
            flutterWebViewPlugin.hide();
            _pickImage(ImageSource.camera);
          });
        } else if (viewState.url.startsWith("https://evibe.in/partner/deliveries/open-gallery")) {
          setState(() {
            flutterWebViewPlugin.stopLoading();
            flutterWebViewPlugin.hide();
            _pickImage(ImageSource.gallery);
          });
        }

        if (viewState.url.startsWith("https://www.instagram.com") ||
            viewState.url.startsWith('https://www.facebook.com') ||
            viewState.url.startsWith('https://m.facebook.com') ||
            viewState.url.startsWith('mailto:') ||
            viewState.url.startsWith('https://www.twitter.com') ||
            viewState.url.startsWith('https://mobile.twitter.com') ||
            viewState.url.startsWith('https://in.pinterest')) {
          setState(() {
            browserRedirect(viewState.url);
            flutterWebViewPlugin.evalJavascript("\$('.loading-screen-gif').css('display', 'none')");
          });
        } else if (viewState.url.startsWith('https://evibe.in/user/logout')) {
          setState(() {
            flutterWebViewPlugin.close();

//            var db = new DatabaseHelper();
//            db.deleteUsers();

            Navigator.pushNamed(context, '/login');
          });
        }
      }

      if (viewState.type == WebViewState.startLoad) {
        if (viewState.url.startsWith('https://you-web-url.in/download')) {
          setState(() {
            flutterWebViewPlugin.evalJavascript("\$('.loading-screen-gif').css('display', 'none')");
          });
        }

        if (viewState.url.startsWith('https://web.whatsapp.com') || viewState.url.startsWith('whatsapp://send') || viewState.url.startsWith('https://apis.whatsapp.com')) {
          setState(() {
            flutterWebViewPlugin.stopLoading();
            flutterWebViewPlugin.goBack();
            _makeURLCall(viewState.url);
          });
        } else if (viewState.url.startsWith('https://evibe.in/partner/profile')) {
          setState(() {
            contents = "My Profile";
          });
        } else if (viewState.url.startsWith('https://evibe.in/partner/messages')) {
          setState(() {
            contents = "Messages";
          });
        } else if (viewState.url.startsWith('https://evibe.in/partner/settlements')) {
          setState(() {
            contents = "Settlements";
          });
        } else if (viewState.url.startsWith('https://evibe.in/partner/packages/live')) {
          setState(() {
            contents = "Packages";
          });
        } else if (viewState.url.startsWith('https://evibe.in/partner/enquiries')) {
          setState(() {
            contents = "Enquiries";
          });
        } else if (viewState.url.startsWith('https://evibe.in/partner/orders')) {
          setState(() {
            contents = "Orders";
          });
        } else if (viewState.url.startsWith('https://evibe.in/partner/deliveries')) {
          setState(() {
            contents = "Deliveries";
          });
        } else if (viewState.url.startsWith('https://evibe.in/partner/dashboard')) {
          setState(() {
            contents = "Dashboard";
          });
        }
      }

      if (viewState.type == WebViewState.finishLoad) {
        print("finish load");
        flutterWebViewPlugin.evalJavascript("\$('.loading-screen-gif').css('display', 'none')");
        flutterWebViewPlugin.evalJavascript("\$('.nav_menu').css('display', 'none')");
        flutterWebViewPlugin.evalJavascript("\$('footer').css('display', 'none')");
      }
    });
  }

  /// Modified onwillpop function, so that we can handle the navigation when user pressed back button/gesture
  Future<bool> onWillPop() async {
    DateTime currentTime = DateTime.now();

    flutterWebViewPlugin.show();
    _scaffoldKey.currentState.openEndDrawer();
    bool backButtonHasNotBeenPressedOrSnackBarHasBeenClosed = backButtonPressTime == null || currentTime.difference(backButtonPressTime) > Duration(seconds: 2);

    if (backButtonHasNotBeenPressedOrSnackBarHasBeenClosed) {
      backButtonPressTime = currentTime;
      flutterWebViewPlugin.goBack();
    }

    return false;
  }

  Future<void> _makeURLCall(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    }
  }

  Future<void> browserRedirect(String url) async {
    flutterWebViewPlugin.stopLoading();
    _makeURLCall(url);
  }

  /// Select an image via gallery or camera
  Future<void> _pickImage(ImageSource source) async {
    File selected = await ImagePicker.pickImage(source: source);

    if (selected == null) {
      print("Image error null");
      /// Show error message
      flutterWebViewPlugin.show();
    } else {
      setState(() {
        _imageFile = selected;
        print('compressedimagesize: ${_imageFile.lengthSync()}');
      });

      String base64Image = base64Encode(_imageFile.readAsBytesSync());
      String fileName = _imageFile.path.split("/").last;

      http.post("http://192.168.0.144:8080/partner/v1/deliveries/62903/upload", body: {
        "attachments": base64Image,
        "name": fileName,
      }).then((res) {
        print(res.statusCode);
        print(fileName);
        flutterWebViewPlugin.show();
        flutterWebViewPlugin.reload();
      }).catchError((err) {
        print(err);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(statusBarColor: Color.fromRGBO(237, 62, 114, 1)));

    /// Top App Bar
    Widget _appBar() => AppBar(
          title: Text(contents),
          leading: IconButton(
            icon: Icon(Icons.menu),
            onPressed: () {
              _scaffoldKey.currentState.openDrawer();
              if (!_scaffoldKey.currentState.isDrawerOpen) {
                flutterWebViewPlugin.hide();
              }
            },
          ),
          backgroundColor: Color(0xffed3e72),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.refresh),
              onPressed: () {
                flutterWebViewPlugin.reload();
              },
            ),
          ],
        );

    /// Left side nav bar
    Widget _drawer() => Drawer(
          child: ListView(
            padding: EdgeInsets.zero,
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.height * 0.1,
                child: DrawerHeader(
                    child: Text(
                      'Evibe.in Partner',
                      style: TextStyle(color: Colors.white, fontSize: 25),
                    ),
                    duration: Duration(milliseconds: 250),
                    decoration: BoxDecoration(gradient: LinearGradient(begin: Alignment.topCenter, end: Alignment.bottomCenter, colors: [Color(0xffed3e72), Color(0xdded3e72)]))),
              ),
              ListTile(
                leading: Icon(Icons.dashboard),
                title: Text('Dashboard'),
                onTap: () {
                  Navigator.pop(context);
                  flutterWebViewPlugin.reloadUrl("https://evibe.in/partner/dashboard");
                },
              ),
              ListTile(
                leading: Icon(Icons.filter_none),
                title: Text('Packages'),
                onTap: () {
                  Navigator.pop(context);
                  flutterWebViewPlugin.reloadUrl("https://evibe.in/partner/packages/live");
                },
              ),
              ListTile(
                leading: Icon(Icons.message),
                title: Text('Messages'),
                onTap: () {
                  Navigator.pop(context);
                  flutterWebViewPlugin.reloadUrl("https://evibe.in/partner/messages");
                },
              ),
              ListTile(
                leading: Icon(Icons.library_books),
                title: Text('Settlements'),
                onTap: () {
                  Navigator.pop(context);
                  flutterWebViewPlugin.reloadUrl("https://evibe.in/partner/settlements");
                },
              ),
              ListTile(
                leading: Icon(Icons.person),
                title: Text('My Profile'),
                onTap: () {
                  Navigator.pop(context);
                  flutterWebViewPlugin.evalJavascript('window.location.replace("https://evibe.in/partner/profile")');
                },
              ),
              ListTile(
                leading: Icon(Icons.exit_to_app),
                title: Text('Logout'),
                onTap: () {
                  Navigator.pop(context);
                  flutterWebViewPlugin.reloadUrl("https://evibe.in/partner/logout");
                },
              ),
            ],
          ),
        );

    ///Bottom navigation bar
    Widget _bottomNavBar() => BottomNavigationBar(
          backgroundColor: Color.fromRGBO(237, 62, 114, 1),
          selectedFontSize: 10.0,
          unselectedFontSize: 10.0,
          items: [
            BottomNavigationBarItem(
              icon: Icon(
                Icons.notifications_active,
                size: 20,
                color: BLACK_LIGHT,
              ),
              title: Text(
                "ENQUIRIES",
                style: TextStyle(fontSize: 16, color: _curIndex == 0 ? BLACK_LIGHT : BLACK_LIGHT),
              ),
//            activeIcon: Icon(
//              Icons.notifications_active,
//              size: 20,
//              color: EVIBE_PINK,
//            ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.event_available,
                size: 20,
                color: BLACK_LIGHT,
              ),
              title: Text(
                "ORDERS",
                style: TextStyle(fontSize: 16, color: _curIndex == 1 ? BLACK_LIGHT : BLACK_LIGHT),
              ),
//            activeIcon: Icon(
//              Icons.event_available,
//              size: 20,
//              color: EVIBE_PINK,
//            ),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.collections,
                size: 20,
                color: BLACK_LIGHT,
              ),
              title: Text(
                "DELIVERIES",
                style: TextStyle(fontSize: 16, color: _curIndex == 2 ? BLACK_LIGHT : BLACK_LIGHT),
              ),
//            activeIcon: Icon(
//              Icons.collections,
//              size: 20,
//              color: EVIBE_PINK,
//            ),
            ),
          ],
          type: BottomNavigationBarType.fixed,
          currentIndex: _curIndex,
          onTap: (index) {
            setState(() {
              _curIndex = index;
              switch (_curIndex) {
                case 0:
                  contents = "Enquiries";
                  flutterWebViewPlugin.reloadUrl("https://evibe.in/partner/enquiries");
                  break;
                case 1:
                  contents = "Orders";
                  flutterWebViewPlugin.reloadUrl("https://evibe.in/partner/orders");
                  break;
                case 2:
                  contents = "Deliveries";
//                flutterWebViewPlugin.reloadUrl("https://evibe.in/partner/deliveries");
//                flutterWebViewPlugin.reloadUrl("http://192.168.0.144:8000/partner/deliveries");
                  flutterWebViewPlugin.evalJavascript('window.location.replace("http://192.168.0.144:8000/partner/deliveries")');
                  break;
                default:
                  contents = "Evibe Partner";
                  break;
              }
            });
          },
        );

    /// Main function to load the webview
    _checkRouteAndOpenWebView(context);
    print("widget calling");

    /// Added custom listener to listen to drawer close event
    _onDrawerClose(bool isDrawerOpen, BuildContext context) {
      if (!isDrawerOpen) {
        flutterWebViewPlugin.show();
      } else {
        flutterWebViewPlugin.hide();
      }
    }

    return Scaffold(
      key: _scaffoldKey,
      body: WillPopScope(
        onWillPop: onWillPop,
        child: Column(
          children: <Widget>[
            if (_imageFile != null) new CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Color(0xFFed3e72))),
          ],
        ),
      ),
      appBar: _appBar(),
      drawerCallback: (bool open) => _onDrawerClose(open, context),
      drawer: _drawer(),
      bottomNavigationBar: _bottomNavBar(),
    );
  }
}
