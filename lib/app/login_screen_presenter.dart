import 'package:partnerappflutter/app/rest_ds.dart';
import 'package:partnerappflutter/app/user.dart';

abstract class LoginScreenContract {
  void onLoginSuccess(User user);

  void onLoginError(String errorTxt);
}

class LoginScreenPresenter {
  LoginScreenContract _view;
  RestDatasource api = new RestDatasource();

  LoginScreenPresenter(this._view);

  doLogin(String username, String password) {
    api.login(username, password).then((User user) {
      _view.onLoginSuccess(user);
    }).catchError((onError) => _view.onLoginError(onError.toString()));
  }

  doSignUp(String name, String username, String password) {
    api.signUp(name, username, password).then((User user) {
      _view.onLoginSuccess(user);
    }).catchError((onError) => _view.onLoginError(onError.toString()));
  }
}
