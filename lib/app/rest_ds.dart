import 'dart:async';

import 'package:partnerappflutter/app/network_util.dart';
import 'package:partnerappflutter/app/user.dart';

class RestDatasource {
  NetworkUtil _netUtil = new NetworkUtil();
  static final baseUrl = "http://apis.evibe.in";
  static final loginUrl = baseUrl + "/customer/v1/login";
  static final signUpUrl = baseUrl + "/customer/v1/signup";
  static final apiKey = "srQfRSdqj40zs0FI";

  Future<User> login(String username, String password) {
    return _netUtil.post(loginUrl, body: {
      "clientId": apiKey,
      "emailId": username,
      "password": password
    }).then((dynamic res) {
      if(res["error"]) {
        throw new Exception(res["error_msg"]);
      }
      return new User.map(res["user"]);
    });
  }

  Future<User> signUp(String name, String username, String password) {
    return _netUtil.post(signUpUrl, body: {
      "clientId": apiKey,
      "name": name,
      "emailId": username,
      "password": password
    }).then((dynamic res) {
      if(res["error"]) {
        throw new Exception(res["error_msg"]);
      }
      return new User.map(res["user"]);
    });
  }
}